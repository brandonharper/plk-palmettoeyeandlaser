<?php

/* modules/bootstrap_layouts/templates/two-columns/bootstrap-6-6/bootstrap-6-6.html.twig */
class __TwigTemplate_aedc05b3ea8b082c62a68236922e5b57dbc58909ce58df4cca6807ecb47e2e50 extends Twig_Template
{
    public function __construct(Twig_Environment $env)
    {
        parent::__construct($env);

        $this->parent = false;

        $this->blocks = array(
        );
    }

    protected function doDisplay(array $context, array $blocks = array())
    {
        $tags = array("if" => 2);
        $filters = array();
        $functions = array();

        try {
            $this->env->getExtension('sandbox')->checkSecurity(
                array('if'),
                array(),
                array()
            );
        } catch (Twig_Sandbox_SecurityError $e) {
            $e->setTemplateFile($this->getTemplateName());

            if ($e instanceof Twig_Sandbox_SecurityNotAllowedTagError && isset($tags[$e->getTagName()])) {
                $e->setTemplateLine($tags[$e->getTagName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFilterError && isset($filters[$e->getFilterName()])) {
                $e->setTemplateLine($filters[$e->getFilterName()]);
            } elseif ($e instanceof Twig_Sandbox_SecurityNotAllowedFunctionError && isset($functions[$e->getFunctionName()])) {
                $e->setTemplateLine($functions[$e->getFunctionName()]);
            }

            throw $e;
        }

        // line 1
        echo "<div class=\"row\">
  ";
        // line 2
        if ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "sidebar_right", array())) {
            // line 3
            echo "  <div class=\"col-sm-6\">
    ";
            // line 4
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "sidebar_right", array()), "html", null, true));
            echo "
  </div>
  ";
        }
        // line 7
        echo "  ";
        if ($this->getAttribute((isset($context["content"]) ? $context["content"] : null), "sidebar_left", array())) {
            // line 8
            echo "  <div class=\"col-sm-6\">
    ";
            // line 9
            echo $this->env->getExtension('sandbox')->ensureToStringAllowed($this->env->getExtension('drupal_core')->escapeFilter($this->env, $this->getAttribute((isset($context["content"]) ? $context["content"] : null), "sidebar_left", array()), "html", null, true));
            echo "
  </div>
  ";
        }
        // line 12
        echo "</div>
";
    }

    public function getTemplateName()
    {
        return "modules/bootstrap_layouts/templates/two-columns/bootstrap-6-6/bootstrap-6-6.html.twig";
    }

    public function isTraitable()
    {
        return false;
    }

    public function getDebugInfo()
    {
        return array (  69 => 12,  63 => 9,  60 => 8,  57 => 7,  51 => 4,  48 => 3,  46 => 2,  43 => 1,);
    }
}
/* <div class="row">*/
/*   {% if content.sidebar_right %}*/
/*   <div class="col-sm-6">*/
/*     {{ content.sidebar_right }}*/
/*   </div>*/
/*   {% endif %}*/
/*   {% if content.sidebar_left %}*/
/*   <div class="col-sm-6">*/
/*     {{ content.sidebar_left }}*/
/*   </div>*/
/*   {% endif %}*/
/* </div>*/
/* */
